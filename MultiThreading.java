/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ADM
 */
public class MultiThreading {
    public static void main(String[] args) {
        /* //inicio do exemplo 1
        multiThreadingThing mt = new multiThreadingThing();
        multiThreadingThing mt2 = new multiThreadingThing();
        //mt.run(); se eu usar esse vai rodar mais não como threads separadas por isso usamos a de baixo.
        mt.start();
        mt2.start();
        //nesse caso acima uma thread é designada a mt e outra para mt2 e ambas executam ao mesmo tempo.
        */ //fim exemplo 1
        /* //inicio do exemplo 2
        for(int i=0;i<3;i++) 
        {
            multiThreadingThing mt = new multiThreadingThing();
            mt.start();
            //agora nesse exemplo temos 3 threads contando até 6 ao mesmo tempo.
        }
        */ //fim do exemplo 2
        /* //inicio do exemplo 3
        for(int i=0;i<3;i++)
        {
            multiThreadingThing2 mt = new multiThreadingThing2(i+1);
            mt.start();
            //Nesse caso vai vir bagunçado porque era esperado que a thread 1 viesse seguido da sua 2 e 3 no entanto
            //não tem garantia qual thread virá primeiro pode ser a sequencia 0,2,1 ou qualquer outra sequência por causa
            //da sua indepencia uma da outra.
            //Se uma thread der algum problema e não devolver o resultado nada interfere no funcionamento das demais
            //porque vão continuar funcionando normalmente exceto a que der erro.
            
        }
        */ //fim do exemplo 3
         //inicio do exemplo 4
         for(int i=0;i<3;i++)
         {
             multiThreadingThing3 mt = new multiThreadingThing3 (i+1);
             mt.start();
             //nesse exemplo dentro do objeto multiThreading3 eu coloquei uma regra para que o 2 ñ execute e pule fora
             //assim eu mostro que independente do que acontecer sempre as demais threads vão se executar mesmo que
             //sua thread vizinha apresente um erro no meio.
         }
         //fim do exemplo 4
         
         /* Existe outros metodo fora o start() que vc pode usar como por exemplo o metodo join() a qual força
         que as threads executem se sequencialmente ñ causando o problema citado nos exemplos anteriores que
         vem bagunçado a ordem.
         Se vc precisa que uma thread termine de ser executada para depois executar a outra pegando o resultado dessa 
         anterior ai sim nesse caso vc usaria o metodo JOIN().
         O metodo isAlive() é responsavel por dizer (nesse exemplo) qual thread está executando retornando 2 valores
         TRUE ou FALSE.
         Esse isAlive() é muito bom usar quando vc precisa verificar se no meio do processamento o sistema operacional
         ñ matou sua thread de proposito cujo o qual ele pode fazer isso para garantir que outras aplicações fiquem
         ativas (maior importancia) e sobre isso é estudado MUITO na disciplina de SISTEMAS OPERACIONAIS.
         */
    }
    
}
